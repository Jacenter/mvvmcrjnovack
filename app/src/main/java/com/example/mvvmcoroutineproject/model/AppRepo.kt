package com.example.mvvmcoroutineproject.model

import android.graphics.Color
import com.example.mvvmcoroutineproject.model.AppRepo.appService
import com.example.mvvmcoroutineproject.model.remote.AppService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlin.random.Random

object AppRepo {

    val appService : AppService = object : AppService {

        override suspend fun generateRandomColor(): Int {
            return Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
        }


        override suspend fun getLifecycleList() = listOf(
            "onCreate",
            "onStart",
            "onResume",
            "onPause",
            "onStop",
            "onRestart",
            "onDestroy",
            "onAttach",
            "onViewCreated",
            "onViewDestroyed",
            "onDetach",
        )
        }


        suspend fun getRandomColor() : Int = withContext(Dispatchers.IO) {
            delay(1000)
            return@withContext appService.generateRandomColor()
        }

        suspend fun retrieveLifecycleList(): List<String> = withContext(Dispatchers.IO) {
            delay(1000)
            return@withContext appService.getLifecycleList()
        }
}