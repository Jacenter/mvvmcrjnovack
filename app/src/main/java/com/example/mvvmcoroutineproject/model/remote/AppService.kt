package com.example.mvvmcoroutineproject.model.remote

interface AppService {

    //function to generate random color
    suspend fun generateRandomColor() : Int

    //function to navigate list of lifecycles
    suspend fun getLifecycleList() : List<String>
}