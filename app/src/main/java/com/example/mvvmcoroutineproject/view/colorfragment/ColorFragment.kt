package com.example.mvvmcoroutineproject.view.colorfragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mvvmcoroutineproject.databinding.FragmentColorBinding
import com.example.mvvmcoroutineproject.viewmodel.ColorFragmentViewModel

class ColorFragment : Fragment() {
    private var _binding : FragmentColorBinding? = null
    private val binding : FragmentColorBinding get() = _binding!!
    val viewModel by activityViewModels<ColorFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentColorBinding.inflate(inflater, container, false).also {
            _binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding) {
        viewModel.randomColor.observe(viewLifecycleOwner) { rndmColor ->
            cvColorViewer.setBackgroundColor(rndmColor)
            cvColorViewer.setOnClickListener {
                val action =
                    ColorFragmentDirections.actionColorFragmentToSpecificColorFragment(rndmColor.toString())
                findNavController().navigate(action)
            }
        }

            btnNext.setOnClickListener{
                val action = ColorFragmentDirections.actionColorFragmentToLifecycleFragment()
                findNavController().navigate(action)
            }

        btnColorRetrieve.setOnClickListener{
            viewModel.generateRandomColor()
        }
    }
}