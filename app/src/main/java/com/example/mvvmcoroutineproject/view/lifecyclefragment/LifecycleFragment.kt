package com.example.mvvmcoroutineproject.view.lifecyclefragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mvvmcoroutineproject.R
import com.example.mvvmcoroutineproject.databinding.FragmentLifecycleBinding
import com.example.mvvmcoroutineproject.databinding.FragmentLifecycleSpecificBinding
import com.example.mvvmcoroutineproject.viewmodel.LifecycleFragmentViewModel

class LifecycleFragment : Fragment() {
    private var _binding : FragmentLifecycleBinding? = null
    private val binding: FragmentLifecycleBinding get() = _binding!!

    private val LifecycleVM by activityViewModels<LifecycleFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentLifecycleBinding.inflate(inflater, container, false)
            .also {
                _binding = it
            }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        topicListener()
        LifecycleVM.state.observe(viewLifecycleOwner) {LifecycleSt ->
            handleState(LifecycleSt)
        }
    }


    private fun handleState(state: LifecycleState) = with(binding) {
        topic.text = state.topic
        btnTopicNxt.isEnabled = state.enableNext
        btnTopicPrv.isEnabled = state.enablePrev

    }

    private fun initViews() = with(binding) {
        btnTopicNxt.setOnClickListener{LifecycleVM.nextTopic()}
        btnTopicPrv.setOnClickListener{LifecycleVM.prevTopic()}


    }

    private fun topicListener() = with(binding) {
        topic.setOnClickListener {
            val topicTitle = topic
            if (topicTitle.text == "onCreate") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onCreate",
                        "Called to do initial creation of a fragment. This is called after onAttach and before onCreateView"
                    )
                findNavController().navigate(action)
            } else if (topicTitle.text == "onStart") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onStart",
                        "Called when the activity is becoming visible to the user. Followed by onResume() if the activity comes to the foreground, or onStop() if it becomes hidden."
                    )
                findNavController().navigate(action)
            } else if (topicTitle.text == "onResume") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onResume",
                        "Called when the activity will start interacting with the user. At this point your activity is at the top of its activity stack, with user input going to it.\n" +
                                "Always followed by onPause()."
                    )
                findNavController().navigate(action)
            } else if (topicTitle.text == "onPause") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onPause",
                        "Called when the activity loses foreground state, is no longer focusable or before transition to stopped/hidden or destroyed state. The activity is still visible to user, so it's recommended to keep it visually active and continue updating the UI. Implementations of this method must be very quick because the next activity will not be resumed until this method returns.\n" +
                                "Followed by either onResume() if the activity returns back to the front, or onStop() if it becomes invisible to the user."
                    )
                findNavController().navigate(action)
            } else if (topicTitle.text == "onStop") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onStop",
                        "Called when the activity is no longer visible to the user. This may happen either because a new activity is being started on top, an existing one is being brought in front of this one, or this one is being destroyed. This is typically used to stop animations and refreshing the UI, etc.\n" +
                                "Followed by either onRestart() if this activity is coming back to interact with the user, or onDestroy() if this activity is going away."
                    )
                findNavController().navigate(action)
            } else if (topicTitle.text == "onRestart") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onRestart",
                        "Called after your activity has been stopped, prior to it being started again.\n" +
                                "Always followed by onStart()"
                    )
                findNavController().navigate(action)
            } else if (topicTitle.text == "onDestroy") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onDestroy",
                        "The final call you receive before your activity is destroyed. This can happen either because the activity is finishing (someone called Activity#finish on it), or because the system is temporarily destroying this instance of the activity to save space. You can distinguish between these two scenarios with the isFinishing() method."
                    )
                findNavController().navigate(action)
            } else if (topicTitle.text == "onAttach") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onAttach",
                        "called once the fragment is associated with its activity."
                    )
                findNavController().navigate(action)
            } else if (topicTitle.text == "onViewCreated") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onViewCreated",
                        "onCreateView is used in fragment to create layout and inflate view. onViewCreated is used to reference the view created by above method. Lastly it is a good practice to define action listener in onActivityCreated."
                    )
                findNavController().navigate(action)
            } else if (topicTitle.text == "onViewDestroyed") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onViewDestroyed",
                        "\n" +
                                "onDestroyView() allows the fragment to clean up resources associated with its View."
                    )
                findNavController().navigate(action)
            } else if (topicTitle.text == "onDetach") {
                val action =
                    LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleSpecificFragment(
                        "onDetach",
                        "onDetach() called immediately prior to the fragment no longer being associated with its activity."
                    )
                findNavController().navigate(action)
            }

        }
    }
}