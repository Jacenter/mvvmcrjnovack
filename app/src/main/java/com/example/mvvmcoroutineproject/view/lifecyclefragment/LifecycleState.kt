package com.example.mvvmcoroutineproject.view.lifecyclefragment

data class LifecycleState (
    val topic: String = "",
    val enablePrev: Boolean = false,
    val enableNext: Boolean = false,
    val isLoading: Boolean = true,
)