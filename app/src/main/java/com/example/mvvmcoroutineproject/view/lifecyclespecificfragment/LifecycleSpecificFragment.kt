package com.example.mvvmcoroutineproject.view.lifecyclespecificfragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.mvvmcoroutineproject.databinding.FragmentLifecycleSpecificBinding

class LifecycleSpecificFragment : Fragment() {
    private var _binding: FragmentLifecycleSpecificBinding? = null
    private val binding: FragmentLifecycleSpecificBinding get() = _binding!!
    private val args by navArgs<LifecycleSpecificFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentLifecycleSpecificBinding.inflate(inflater, container, false)
            .also {
                _binding = it
            }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initListeners() = with(binding){
        btnBack.setOnClickListener{
            val action = LifecycleSpecificFragmentDirections.actionLifecycleSpecificFragmentToLifecycleFragment()
            findNavController().navigate(action)
        }
    }

    private fun initViews() = with(binding){
        lifecycleNameView.text = args.lifecycleName
        lifecycleDetailView.text = args.lifecycleDetails
    }
}