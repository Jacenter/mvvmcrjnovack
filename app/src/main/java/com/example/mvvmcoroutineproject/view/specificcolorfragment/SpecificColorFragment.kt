package com.example.mvvmcoroutineproject.view.specificcolorfragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.mvvmcoroutineproject.R
import com.example.mvvmcoroutineproject.databinding.FragmentSpecificColorBinding
import com.example.mvvmcoroutineproject.viewmodel.ColorFragmentViewModel

class SpecificColorFragment : Fragment() {
    private var _binding: FragmentSpecificColorBinding? = null
    private val binding: FragmentSpecificColorBinding get() = _binding!!
    private val args by navArgs<SpecificColorFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return FragmentSpecificColorBinding.inflate(inflater, container, false)
            .also {
                _binding = it
            }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.root.setBackgroundColor(args.randomColor.toInt())
        initListeners()

    }

    private fun initListeners() = with(binding) {
        btnBack.setOnClickListener{
            val action = SpecificColorFragmentDirections.actionSpecificColorFragmentToColorFragment()
            findNavController().navigate(action)
        }
    }
}