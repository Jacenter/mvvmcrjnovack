package com.example.mvvmcoroutineproject.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mvvmcoroutineproject.model.AppRepo
import kotlinx.coroutines.launch

class ColorFragmentViewModel : ViewModel() {

    private val appRepo = AppRepo

    private val _randomColor: MutableLiveData<Int> = MutableLiveData<Int>()
    val randomColor : LiveData<Int> get() = _randomColor

    fun generateRandomColor() = viewModelScope.launch{
        _randomColor.value = appRepo.getRandomColor()
    }
}