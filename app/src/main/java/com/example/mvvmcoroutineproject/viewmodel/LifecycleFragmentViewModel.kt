package com.example.mvvmcoroutineproject.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mvvmcoroutineproject.model.AppRepo
import com.example.mvvmcoroutineproject.view.lifecyclefragment.LifecycleState
import kotlinx.coroutines.launch

class LifecycleFragmentViewModel : ViewModel() {
    private val appRepo = AppRepo
    private var lifecycleMethods : List<String> = emptyList()
    private var currentListIndex: Int = 0

    private val enableNext get() = lifecycleMethods.size.minus(1)>currentListIndex
    private val enablePrev get() = currentListIndex > 0

    private val _state = MutableLiveData(LifecycleState())
    val state: LiveData<LifecycleState> get() = _state

    init {
        viewModelScope.launch {
            lifecycleMethods = appRepo.retrieveLifecycleList()
            updateState()
        }
    }

    private fun updateState() {
        _state.value = LifecycleState(
            topic = lifecycleMethods[currentListIndex],
            enableNext = enableNext,
            enablePrev = enablePrev,
            isLoading = false
        )
    }

    fun nextTopic() {
        if (enableNext) {
            currentListIndex++
            updateState()
        }
    }

    fun prevTopic() {
        if (enablePrev){
            currentListIndex--
            updateState()
        }
    }
}